function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  return {
    total: t,
    hours: hours,
    minutes: minutes,
    seconds: seconds
  };
}



$(function(){
  if ($('.hours').length) {
    function initializeClock(id, endtime) {
      var clock = document.getElementById(id);
      var hoursSpan = clock.querySelector(".hours");
      var minutesSpan = clock.querySelector(".minutes");
      var secondsSpan = clock.querySelector(".seconds");

      function updateClock() {
        var t = getTimeRemaining(endtime);

        if (t.total <= 0) {
          document.getElementById("countdown").className = "hidden";
          document.getElementById("deadline-message").className = "visible";
          document.getElementById("test_main").className = "hidden";

          clearInterval(timeinterval);
          return true;
        }

        hoursSpan.innerHTML = ("0" + t.hours).slice(-2);
        minutesSpan.innerHTML = ("0" + t.minutes).slice(-2);
        secondsSpan.innerHTML = ("0" + t.seconds).slice(-2);
      }

      updateClock();
      var timeinterval = setInterval(updateClock, 1000);
    }

    var deadline = new Date(Date.parse(new Date()) + 1800 * 1000);
    initializeClock("countdown", deadline);
  }

  $('.step_main_test .images').lightGallery({
    thumbnail: false,
    selector: 'a',
    download: false,
    autoplayControls: false,
    zoom: false,
    fullScreen: false,

  });

});

$(document).ready(function (e) {
  $(".sortable").sortable();
  $(".sortable").disableSelection();

  $(".btn_next").on("click", function () {
    if ($('.sortable_step').hasClass('active_step')) {
      var show_value = "";
      $(".sortable li").each(function (index, element) {
        show_value = show_value + $(this).text() + '\n';
      });
      $(".sortable_step.active_step .sort_show_value").val(show_value);
    }

    $('.test_main .step_main_test.active_step').next().addClass('active_step');
    $('.test_main .step_main_test.active_step').prev().removeClass('active_step');
  });

  $('.bind_list li').on('click', function(){
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
      $(this).closest('.bind_list').addClass('disabled');
    }
  });

  let bindValue = '';

  $('.btn_bind').on('click', function(){

    let leftText = $('.left_list li.active').text();
    let rightText = $('.right_list li.active').text();

    bindValue = bindValue + leftText + '-' + rightText + '\n';

    $('.bind_show_value').val(bindValue);

    if ($('.left_list li').hasClass('active') && $('.left_list').hasClass('disabled')){
      
      $('.left_list li.active').addClass('bg1');

      $('.left_list li').removeClass('active');
      $('.left_list').removeClass('disabled');
    }

    if ($('.right_list li').hasClass('active') && $('.right_list').hasClass('disabled')){
      $('.right_list li.active').addClass('bg1');
      
      $('.right_list li').removeClass('active');
      $('.right_list').removeClass('disabled');
    }
  });

  $('.help_rating_form .btn,.close_help').on('click', function(e){
    e.preventDefault();
    if( $('.help_rating_form').hasClass('opened')) {
      $('.help_rating_form').removeClass('opened');
    }
  });

  $('.btn_check').on('click', function(){
    if( $(this).find('input').is(':checked')) {
      $('.help_rating_form .btn').trigger('click');
    }
  });

  $('.rev_btn').on('click', function(){
    $('.help_comment').slideDown();
    $(this).parent().slideUp();
  });

  $(document).ready(function(){
    setTimeout(function(){
      if(!$('.form-popup-wrapper.red_form').hasClass('opened')) {
        $('.form-popup-wrapper.red_form').addClass('opened');
        $('html').addClass('.oveflowHidden');
      }
    }, 5000);
  });
});

